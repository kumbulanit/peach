module "eks-alb" {
  source            = "app.terraform.io/tfg-labs/eks-alb/aws"
  version           = "1.0.0"
  region            = var.region
  vpc_id            = var.vpc_id
  subnet            = var.subnet
  pub_subnet        = var.pub_subnet
  environment       = var.environment
  access_log_bucket = var.access_log_bucket
  elb-name          = "eks-public"
  eks_cluster_name  = "peach"
  cloudflare_sg     = var.cloudflare_sg
  appname           = "eks-public-ingress"
}
