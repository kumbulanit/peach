variable "region" {}
variable "vpc_id" {}
variable "environment" {}
variable "access_log_bucket" {}
variable "subnet" {
  type = list(any)
}
variable "pub_subnet" {
  type = list(any)
}
variable "cloudflare_sg" {}
