resource "aws_lb" "default" {
  name                       = var.alb_name
  internal                   = true
  load_balancer_type         = "application"
  security_groups            = var.security_groups
  subnets                    = var.alb_subnet
  enable_deletion_protection = true
  drop_invalid_header_fields = true
  access_logs {
    bucket  = var.access_log_bucket
    prefix  = "alb-${var.environment}-${var.alb_name}"
    enabled = true
  }
  tags = {
    Name        = "alb-${var.environment}-${var.alb_name}"
    environment = var.environment
    appname     = var.appname
    owner       = var.owner
    costcenter  = var.costcenter
    team        = var.team
  }
}

resource "aws_lb_target_group" "https" {
  name        = "peach-https-${var.environment}-${var.alb_name}"
  port        = 443
  protocol    = "HTTPS"
  target_type = "instance"
  vpc_id      = var.vpc_id
  }
  health_check {
    path     = var.health_check_path
    protocol = "HTTPS"
  }
}

resource "aws_lb_listener" "http" {
  #checkov:skip=CKV_AWS_103:"Ensure that load balancer is using TLS 1.2 -  We using 80 and only allow cloudflare where tls terminated"
  #checkov:skip=CKV_AWS_2:"Ensure ALB protocol is HTTPS -  We using 80 and only allow cloudflare where tls terminated"
  load_balancer_arn = aws_lb.default.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

}
resource "aws_lb_listener" "https" {
  load_balancer_arn = aws_lb.default.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  certificate_arn   = var.certificate_arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.https.arn
  }

}

resource "aws_alb_target_group_attachment" "default" {
  target_group_arn = aws_lb_target_group.https.arn
  target_id        = var.target_id
  port             = 443
}

