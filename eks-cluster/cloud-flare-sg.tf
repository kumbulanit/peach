resource "aws_security_group" "allow_cloudflare" {
  #checkov:skip=CKV2_AWS_5:"Ensure that Security Groups are attached to another resource - it is attached in this resource module.elb.module.elb.aws_lb.default"
  name        = "cloudflare-sg"
  description = "Allow Cloudflare inbound traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "CloudFlare"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["10.18.102.0/24"]
  }
}