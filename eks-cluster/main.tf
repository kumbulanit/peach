module "eks" {
  source           = "terraform-aws-modules/eks/aws"
  version          = "17.24.0"
  cluster_name     = var.cluster_name
  cluster_version  = var.cluster_version
  subnets          = var.subnet
  manage_aws_auth  = false
  enable_irsa      = true
  write_kubeconfig = false

  tags = {
    Environment = "tfg-eks"
    GithubRepo  = "terraform-aws-eks"
    GithubOrg   = "terraform-aws-modules"
  }

  vpc_id = var.vpc_id

}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

resource "aws_eks_node_group" "nodegroup" {
  cluster_name           = var.cluster_name
  disk_size              = var.disk_size
  node_group_name_prefix = var.cluster_name
  node_role_arn          = var.node_role_arn
  subnet_ids             = var.subnet
  instance_types         = var.instance_type
  capacity_type          = var.capacity_type
  depends_on             = [module.eks.cluster_id]
  labels                 = {}

  tags = {
    Name        = "eks-${var.cluster_name}-node"
    environment = var.environment
    team        = var.team
  }

  scaling_config {
    desired_size = var.desired_size
    max_size     = var.max_size
    min_size     = var.min_size
  }

  update_config {
    max_unavailable = var.max_unavailable
  }

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [scaling_config[0].desired_size, scaling_config[0].min_size]
  }

}

resource "aws_autoscaling_group_tag" "autoscaling_group_tag" {

  autoscaling_group_name = aws_eks_node_group.nodegroup.resources[0]["autoscaling_groups"][0]["name"]

  tag {
    key   = "Name"
    value = "eks-${var.cluster_name}-node"

    propagate_at_launch = true
  }
}
