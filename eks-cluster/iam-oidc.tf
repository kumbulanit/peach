data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

locals {
  k8s_service_account_namespace = "peach"
  k8s_service_account_name      = "peach-sa"
}

module "iam_assumable_role_admin" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version = "~> 4.0"

  create_role                   = true
  role_name                     = "k8s-applicaion-role"
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.k8s.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:${local.k8s_service_account_namespace}:${local.k8s_service_account_name}", "system:serviceaccount:peach-staging:${local.k8s_service_account_name}"]
}

resource "aws_iam_policy" "k8s" {
  name_prefix = "k8s-app-secrets"
  policy      = data.aws_iam_policy_document.k8s.json
}

data "aws_iam_policy_document" "k8s" {
  #checkov:skip=CKV_AWS_108: "Ensure IAM policies does not allow data exfiltration"

  statement {
    sid    = "peachserviceaccount"
    effect = "Allow"

    actions = [
      "secretsmanager:GetSecretValue",
      "secretsmanager:DescribeSecret",
    ]

    resources = ["*"]
  }

}
