variable "region" {}
variable "vpc_id" {}
variable "environment" {}
variable "subnet" {
  type = list(any)
}
variable "cluster_name" {}
variable "cluster_version" {}
variable "instance_type" {
  type = list(any)
}
variable "disk_size" {}
variable "capacity_type" {}
variable "asg_desired_capacity" {}
variable "desired_size" {}
variable "max_size" {}
variable "min_size" {}
variable "max_unavailable" {}
variable "node_role_arn" {}
variable "team" {}
