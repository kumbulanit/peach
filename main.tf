module "eks-cluster" {
  source               = "./eks-cluster"
  region               = var.region
  vpc_id               = var.vpc_id
  subnet               = var.subnet
  environment          = var.environment
  cluster_name         = "peach"
  cluster_version      = "1.21"
  instance_type        = ["t3.medium"]
  disk_size            = "100"
  capacity_type        = "ON_DEMAND"
  asg_desired_capacity = 2
  desired_size         = 2
  max_size             = 4
  min_size             = 2
  max_unavailable      = 1
  node_role_arn        = "arn:aws:iam::464208442201:role/eksctl-prd-eu-west-1-eks-nodeg-NodeInstanceRole"
}












