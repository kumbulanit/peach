variable "AWS_ACCESS_KEY_ID" {}
variable "AWS_SECRET_ACCESS_KEY" {}
variable "region" {}
variable "vpc_id" {}
variable "subnet" {
  type = list(any)
}
